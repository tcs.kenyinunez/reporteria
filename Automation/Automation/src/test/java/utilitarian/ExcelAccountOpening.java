package utilitarian;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import utilitarian.Excel;

// Clase que contiene los campos del excel ha ser usado en la clase Test
public class ExcelAccountOpening extends Excel{
	
	// Ubicación del pool de datos
	public static String nombreArchivo = "C:\\Users\\T01914\\Desktop\\Automation\\Automation\\src\\test\\resources\\data\\ExcelAccountOpenning.xlsx";
	// Variables de la Cuenta
	private int nroCorrelativo;
	private String campo;
//	private String Doc_Identidad;
//	private String Num_Identidad;
//	private String Tipo_Cuenta;
//	private String Modalidad;
//	private String Moneda;
//	private String Tipo_Sub_Producto;
//	private String Chequera;
//	private String Nombre_Cuenta;
//	private String Sucursal_Apertura;
//	private String Agencia_Apertura;

	
	// Getters and Setters de la cuenta
	public int getNroCorrelativo() {
		return nroCorrelativo;
	}
	public void setNroCorrelativo(int nroCorrelativo) {
		this.nroCorrelativo = nroCorrelativo;
	}
	
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	//	public String getDoc_Identidad() {
//		return Doc_Identidad;
//	}
//	public void setDoc_Identidad(String doc_Identidad) {
//		Doc_Identidad = doc_Identidad;
//	}
//	public String getNum_Identidad() {
//		return Num_Identidad;
//	}
//	public void setNum_Identidad(String num_Identidad) {
//		Num_Identidad = num_Identidad;
//	}
//	public String getTipo_Cuenta() {
//		return Tipo_Cuenta;
//	}
//	public void setTipo_Cuenta(String tipo_Cuenta) {
//		Tipo_Cuenta = tipo_Cuenta;
//	}
//	public String getModalidad() {
//		return Modalidad;
//	}
//	public void setModalidad(String modalidad) {
//		Modalidad = modalidad;
//	}
//	public String getMoneda() {
//		return Moneda;
//	}
//	public void setMoneda(String moneda) {
//		Moneda = moneda;
//	}
//	public String getTipo_Sub_Producto() {
//		return Tipo_Sub_Producto;
//	}
//	public void setTipo_Sub_Producto(String tipo_Sub_Producto) {
//		Tipo_Sub_Producto = tipo_Sub_Producto;
//	}
//	public String getChequera() {
//		return Chequera;
//	}
//	public void setChequera(String chequera) {
//		Chequera = chequera;
//	}
//	public String getNombre_Cuenta() {
//		return Nombre_Cuenta;
//	}
//	public void setNombre_Cuenta(String nombre_Cuenta) {
//		Nombre_Cuenta = nombre_Cuenta;
//	}
//	public String getSucursal_Apertura() {
//		return Sucursal_Apertura;
//	}
//	public void setSucursal_Apertura(String sucursal_Apertura) {
//		Sucursal_Apertura = sucursal_Apertura;
//	}
//	public String getAgencia_Apertura() {
//		return Agencia_Apertura;
//	}
//	public void setAgencia_Apertura(String agencia_Apertura) {
//		Agencia_Apertura = agencia_Apertura;
//	}
	// Lectura de cada campo en el excel
	public static ArrayList<ExcelAccountOpening> leerExcel() throws IOException{
		fi = new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		int rowCount = ws.getLastRowNum();
		System.out.println("Se encontraron " + (rowCount -4) + " registros");
		ArrayList<ExcelAccountOpening> arrayObject = new ArrayList<ExcelAccountOpening>();
		for(int indexRow=5;indexRow<=rowCount;indexRow++) {
			row = ws.getRow(indexRow);
			ExcelAccountOpening tcParameters = new ExcelAccountOpening();
			DataFormatter formatter = new DataFormatter();
			tcParameters.setNroCorrelativo(Integer.parseInt(formatter.formatCellValue(row.getCell(0))));
			tcParameters.setCampo(formatter.formatCellValue(row.getCell(1)));
//			tcParameters.setDoc_Identidad(formatter.formatCellValue(row.getCell(1)));
//			tcParameters.setNum_Identidad(formatter.formatCellValue(row.getCell(2)));
//			tcParameters.setTipo_Cuenta(formatter.formatCellValue(row.getCell(3)));
//			tcParameters.setModalidad(formatter.formatCellValue(row.getCell(4)));
//			tcParameters.setMoneda(formatter.formatCellValue(row.getCell(5)));
//			tcParameters.setTipo_Sub_Producto(formatter.formatCellValue(row.getCell(6)));
//			tcParameters.setChequera(formatter.formatCellValue(row.getCell(7)));
//			tcParameters.setNombre_Cuenta(formatter.formatCellValue(row.getCell(8)));
//			tcParameters.setSucursal_Apertura(formatter.formatCellValue(row.getCell(9)));
//			tcParameters.setAgencia_Apertura(formatter.formatCellValue(row.getCell(10)));
			arrayObject.add(tcParameters);
		}
		wb.close();
		fi.close();
		return arrayObject;
	}
	
	// Establecer el valor de la celda en el excel
	public static void establecerValorCelda(int indRow, int indCell, String data) throws IOException {
		fi= new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		row=ws.getRow(indRow);
		cell=row.createCell(indCell);
		cell.setCellValue(data);
		fo=new FileOutputStream(nombreArchivo);
		wb.write(fo);		
		wb.close();
		fi.close();
		fo.close();
	}
}