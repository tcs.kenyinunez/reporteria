package utilitarian;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NegocioCompartido {
	
	// Configuración de la ruta del reporte
	public String getDirectorioProyecto() {
		String directorio = System.getProperty("user.dir");
		directorio = directorio.replace("\\", "\\\\");
		System.out.println(directorio);
		return directorio;
	}
	
	// Obtener hora del reporte
	public String getHoraExacta() {
		Date date = new Date();
		DateFormat hourdateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String hora = hourdateFormat.format(date);
		hora = hora.replaceAll(":", "");
		hora = hora.replaceAll("/", "");
		hora = hora.replaceAll(" ", "_");
		System.out.println(hora);
		return hora;
	}
}
