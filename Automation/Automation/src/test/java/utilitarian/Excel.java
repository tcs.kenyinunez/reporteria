package utilitarian;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

// Clase que es extendida en las clases que contienen el pool de datos tanto para flash, web y desktop
public class Excel {
	
	// Variables requeridas para configurar el excel
	public static String nombreArchivo="";
	public static FileInputStream fi;
	public static FileOutputStream fo;
	public static XSSFWorkbook wb;
	public static XSSFSheet ws;
	public static XSSFRow row;
	public static XSSFCell cell;
	
	// Variables complementarias
	protected String caso;
	protected String flag;
	protected String mensaje;
	
	// Getters and Setters del excel
	public String getCaso() {
		return caso;
	}
	public void setCaso(String caso) {
		this.caso = caso;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	// Lectura del excel
	public static void leerExcel(String nombreArchivo) throws IOException {
		fi = new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Entrada");
	}
	
	// Comparaci�n por fila del excel
	public static int compareByRow(String nombreArchivo,int indexCell ,Object compare) throws IOException {
		leerExcel(nombreArchivo);
		int rowCount = ws.getLastRowNum();
		int registersFound = 0;
		for(int indexRow=5; indexRow<=rowCount;indexRow++) {
			row = ws.getRow(indexRow);
			DataFormatter formatter = new DataFormatter();
			if(formatter.formatCellValue(row.getCell(indexCell)).equals(compare)) {
				registersFound++;
			}
		}
		wb.close();
		fi.close();
		return registersFound;
	}
	
	// Obtener valor celda del excel
	public static String obtenerValorCelda(String nombre,int indRow,int indCell) throws IOException {
		fi=new FileInputStream(nombre);
		wb=new XSSFWorkbook(fi);
		ws=wb.getSheet("Hoja1");
		if(ws.getRow(indRow)==null) {
			row= ws.createRow(indRow);
		}else {
			row=ws.getRow(indRow);
		}
		cell=row.getCell(indCell);
		String data;
		try 
		{
			DataFormatter formatter = new DataFormatter();
            String cellData = formatter.formatCellValue(cell);
            return cellData;
		}
		catch (Exception e) 
		{
			data="";
		}
		wb.close();
		fi.close();
		return data;
	}
	
	// Establecer valor de la celda del excel
	public static void establecerValorCelda(String nombre, int indRow, int indCell, String data) throws IOException {
		fi= new FileInputStream(nombre);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		if(ws.getRow(indRow)==null) {
			row= ws.createRow(indRow);
		}else {
			row=ws.getRow(indRow);
		}
		cell=row.createCell(indCell);
		cell.setCellValue(data);
		fo=new FileOutputStream(nombre);
		wb.write(fo);		
		wb.close();
		fi.close();
		fo.close();
	}
	
	// Obtener �ltimo n�mero de la fila por libro
	public static int getNumLastRowByBook(String nombre) throws IOException {
		fi= new FileInputStream(nombre);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		int numLastRow = ws.getLastRowNum();
		wb.close();
		fi.close();
		return numLastRow;
	}
}
