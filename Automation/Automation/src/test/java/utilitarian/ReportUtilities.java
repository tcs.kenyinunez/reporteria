package utilitarian;

import java.awt.Desktop;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ReportUtilities {
	
	// Instancias del extentReports
	ExtentHtmlReporter htmlReporter;
	ExtentReports extent;
	ExtentTest test;
	
	// Configuración del reporte
	public void setUp(String nombreDirectorio, String docTitle) {
		File directorio = new File(nombreDirectorio);
		directorio.mkdir();
		File directorioScreenShots = new File(nombreDirectorio + "\\screenshots\\");
		directorioScreenShots.mkdir();
		htmlReporter = new ExtentHtmlReporter(nombreDirectorio  + "\\report.html");
		htmlReporter.config().setDocumentTitle(docTitle);
		htmlReporter.config().setReportName(docTitle);
		htmlReporter.config().setEncoding("UTF-8");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
	}
	
	public void test(String titleReport, String descriptionReport) {
		test = extent.createTest(titleReport,descriptionReport);
	}
	
	public ExtentTest getTestObject() {
		return this.test;
	}
	
	// Mostrar el reporte al finalizar la ejecución
	public void terminarReporte(String nombreArchivo) {
		try {
			extent.flush();
			File reporte = new File(nombreArchivo);
			Desktop.getDesktop().open(reporte);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// Capturar una parte de la imagen total para mostrarlo en el reporte
	public String TakesScreenshot(String route, WebDriver driver) throws Exception
    {
		if(driver != null) {
			File scrFile = ((org.openqa.selenium.TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(scrFile, new File(route));
		}else {
			Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
			BufferedImage capture = new Robot().createScreenCapture(screenRect);
			ImageIO.write(capture, "png", new File(route));
		}
		String strRoute = route;
		return strRoute;
    }
	
	// Capturar la imagen total para mostrarlo en el reporte
	public void getScreenShotWithState(Status status,String message,String route,WebDriver driver) {
		try {
			test.log(status, message, MediaEntityBuilder.createScreenCaptureFromPath(TakesScreenshot(route,driver)).build());
			test.addScreenCaptureFromPath(route);
		}catch(Exception e) {
			e.printStackTrace();
		}
			
	}
	
	// Crear el directorio donde se va a alojar el reporte
	public void crearDirectivo() {
		File directorio = new File("reportes");
		directorio.mkdir();
	}
}
