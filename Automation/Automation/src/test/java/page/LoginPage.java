package page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

	public static WebDriver driver;
	
	public LoginPage(WebDriver driver){
		this.driver = driver;
	}
	
	// PASOS DE PRUEBAS
	
	public void ClickCertificacion() throws InterruptedException{
		driver.findElement(By.xpath("//*[@id='featured-v2']/a/span/strong")).click();
		Thread.sleep(500);
	}
	
	public void IngresarNomUsuario() throws InterruptedException{
		driver.findElement(By.xpath("//*[@id='id01']/form/div[2]/input[1]")).sendKeys("S80973");
		Thread.sleep(500);
	}
	
	public void IngresarNomContrasena() throws InterruptedException{
		driver.findElement(By.xpath("//*[@id='id01']/form/div[2]/input[2]")).sendKeys("miche118");
		Thread.sleep(500);
	}
	
	public void enterLogin() throws InterruptedException{
		driver.findElement(By.xpath("//*[@id='id01']/form/div[2]/button[1]")).click();
		Thread.sleep(500);
	}
	
}
